<?php

// ============== INITIALIZES =============
require_once('config.php');

// $username = "admin";
// $password = // getenv('WIM_DB_PASSWORD');

setlocale(LC_TIME, "de_DE.utf8");
$servername = "localhost";
$db="wp_twitteducation";


// Create connection
$conn = new mysqli($servername, $username, $password,$db);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$conn->set_charset("utf8");

$sql = "SELECT project FROM wim_projects where active='1';"; 
$res_active = $GLOBALS["conn"]->query($sql);
while($row = $res_active->fetch_assoc()) {
    $active_project=$row["project"];
}

$sql = " SELECT * FROM wim_configuration where configuration_project='$active_project';"; 
$resconf = $GLOBALS["conn"]->query($sql);
while($row = $resconf->fetch_assoc()) {
    if ("hint_root_url" == $row["configuration_key"]) {$hint_root_url=$row["configuration_value"];}
    if ("mode" == $row["configuration_key"]) {$mode=$row["configuration_value"];}
    if ("wim_url_subdirectoy" == $row["configuration_key"]) {$wim_url_subdirectoy=$row["configuration_value"];}
}

ini_set('display_errors', 1);
require_once('TwitterAPIExchange.php');

//=========================================

processURLParameters();

function processURLParameters () {
    if (isset($_GET["command"])) processCommand($_GET["command"]);
    if (isset($_GET["setProject"])) setProject($_GET["setProject"]);
    if (false == strpos($_SERVER['REQUEST_URI'], '?')) showTweetPreparation();
}

function setProject($project) {
    $GLOBALS["active_project"] = $project;
    $sql="UPDATE `wim_projects` SET `active` = '0' WHERE `wim_projects`.`active` = 1;";
    $result = $GLOBALS["conn"]->query($sql);
    $sql="UPDATE `wim_projects` SET `active` = '1' WHERE `wim_projects`.`project` = '$project';"; 
    $result = $GLOBALS["conn"]->query($sql);
    decoratePage();
}

function processCommand($command) {  
    if ("showNextTweet" == $command) {
        showTweetPreparation();
        BuilTwitterStatusFromPOSTArguments();
    } elseif ("send_protocol" == $command) {
        showSendProtocol();
    } elseif ("parse_send_protocol" == $command) {
        retrieveTwitterAnswerFromSentTweet_updateAndShowSendTweetEntryWithIt();
    } elseif ("getTwitterInfos" == $command) {
        getTwitterInfos();
    } elseif ("getTwitterInfos_Answers" == $command) {
        getTwitterInfos_Answers();    
    } elseif ("getTwitterInfos_RelevantRetweets" == $command) {
        getTwitterInfos_RelevantRetweets();
    } elseif ("getTwitterInfos_Helpers" == $command) {
        getTwitterInfos_Helpers();
    } elseif ("getTwitterInfos_UsersID" == $command) {
        getTwitterInfos_UsersID();
    } elseif ("getTwitterInfos_Followers" == $command) {
        getTwitterInfos_Followers();
    } elseif ("getTwitterInfos_UsersName" == $command) {
        getTwitterInfos_UsersName();
    } elseif ("showStatisticsOverview" == $command) {
        showStatisticsOverview();
    } elseif ("showAnswers" == $command) {
        showAnswers();    
    } elseif ("getTwitterInfos_MentionsFromConversaton_ID" == $command) {
        getTwitterInfos_MentionsFromConversaton_ID();    
    } elseif ("showFactsAndQuestions" == $command) {
        showFactsAndQuestions();
    } elseif ("updateEvaluation" == $command) {
        updateEvaluation();
    } else {
        echo "unknown command";
    }
} 

function updateEvaluation() {
    echo "<br>";
    $sql="update wim_twitter_answers set answer_evaluation='" . $_POST["evaluation"] . "' where id='" . $_POST["answer_id"] . "';";
    $result = $GLOBALS["conn"]->query($sql);
    showAnswers();
}

function retrieveTwitterAnswerFromSentTweet_updateAndShowSendTweetEntryWithIt(){
    decoratePage();
    if (isset($_GET["protocol_id"])) {
        $sql="select * from wim_send_tweets where id=" . $_GET["protocol_id"] . " and project='" . $GLOBALS["active_project"] . "';";
        $result = $GLOBALS["conn"]->query($sql);
        echo '<table border="1">';
        while($row = $result->fetch_assoc()) {
            $updateResult = updateSendTweetEntryWithInfoFromTwitterAPI_JSONAnswer($row["twitter_api_response"],$_GET["protocol_id"]);
            echo "<tr><td>Update Success</td><td>" . $updateResult[2] ."</td></tr>";
            echo "<tr><td>Send Tweet ID</td><td>" . $_GET["protocol_id"] ."</td></tr>";
            echo "<tr><td>Status</td><td>" . $updateResult[0] ."</td></tr>";
            echo "<tr><td>Conversation Id</td><td>" . $updateResult[1] ."</td></tr>";
            echo "<tr><td>Timestamp</td><td>" . $row["timestamp"] ."</td></tr>";
            echo "<tr><td>Question Id</td><td>" . $row["question_id"] ."</td></tr>";
            echo "<tr><td>Question</td><td>" . $row["question"] ."</td></tr>";
            echo "<tr><td>Recipients</td><td>" . $row["recipients"] ."</td></tr>";
            echo "<tr><td>Followers</td><td>" . $row["first_degree_user_id"] ."</td></tr>";
            echo "<tr><td>Not Followers</td><td>" . $row["second_degree_user_info"] ."</td></tr>";
            echo "<tr><td>Hashtag</td><td>" . $row["hashtag"] ."</td></tr>";
            echo "<tr><td>Twitter API Response</td><td>" . $row["twitter_api_response"] ."</td></tr>";
        } 
        echo "</table>";
    }
}

function updateSendTweetEntryWithInfoFromTwitterAPI_JSONAnswer($twitterAPI_JSONAnswer,$protocol_id){
    $parsedResult = analyseTwitterAPI_JSONAnswer($twitterAPI_JSONAnswer);
    $sql="update wim_send_tweets set status='" . $parsedResult[0] . "',conversation_id=" . $parsedResult[1] . " where id=" . $protocol_id . ";";
    $result = $GLOBALS["conn"]->query($sql); 
    array_push($parsedResult,"$result");
    return $parsedResult;
}

function analyseTwitterAPI_JSONAnswer($twitterAPI_JSONAnswer){
    $id_str = getConversationIDFromTwitterAPI_JSONAnswer($twitterAPI_JSONAnswer);
    $conversation_id="";
    if("JSON_Parse_Error"==$id_str) {$status='json_parse_error';}
    elseif("Test_Mode" ==$id_str) {$status='test_mode';}
    elseif(ctype_digit($id_str)) {$status='processed';$conversation_id="'$id_str'";}
    else {$status='invalid_id_str';}
    return array ($status,$conversation_id);
}

function getConversationIDFromTwitterAPI_JSONAnswer($twitterAPI_JSONAnswer) {
    $cleansedAnswer=removeKnownMistakeInTwitterAPI_JSONAnswer($twitterAPI_JSONAnswer);
    $deserializedTwitterAPIAnswer=json_decode($cleansedAnswer,true);
    if(isset($deserializedTwitterAPIAnswer["id"])) {
        return $deserializedTwitterAPIAnswer["id"];
    } else {
        return "JSON_Parse_Error";
    } 
}

function removeKnownMistakeInTwitterAPI_JSONAnswer($TwitterAPI_JSONAnswer){
    $faultyLineToRemove='"source".*au003e",';
    return preg_replace("/$faultyLineToRemove/",'',$TwitterAPI_JSONAnswer);
}

function showStatisticsOverview() {
    decoratePage();
    showPoliticianInteractionStatisticsTable();
    showPostedMentionsPerDayTable();
    showInteractionStatisticsTable();
    showUserInteractionStatisticsTable();
    showUserQuestionInteractionStatisticsTable();
    showUserQuestioAnswerTypeStatisticsTable();
}

function showPostedMentionsPerDayTable() {
    $sql1='
SELECT 
	date,
	count(t2.id) as "Kontakte"
FROM 
	wim_twitter_accounts t1
	JOIN wim_twitter_mentions t2 
	ON lower(t1.twitter_name) = lower(t2.twitter_name_receiver)
WHERE  
	t1.twitter_follower=0 
	and t1.type="candidate"
	and t1.active=1
    and t1.project="' . $GLOBALS["active_project"] . '" 
group by date
order by date desc
';
    $result1 = $GLOBALS["conn"]->query($sql1);
    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>date</th>
            <th>Kontakte</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result1->fetch_assoc()) {
        echo "<tr>
        <td>" . $row["date"] . "</td>
        <td>" . $row["Kontakte"] . "</td>
        </tr>\n\r";
    }
    echo "</tbody></table>\n\r";
}


function showPoliticianInteractionStatisticsTable() {
    $sql1='
    select 
        "Kontakte", 
        count(*) as Anzahl
    from 
        `wim_twitter_mentions` 
        join wim_twitter_accounts on lower(wim_twitter_mentions.twitter_name_receiver)=lower(wim_twitter_accounts.twitter_name)
    where 
        wim_twitter_accounts.type="candidate" 
        and wim_twitter_mentions.project="' . $GLOBALS["active_project"] . '"
    union
    select 
        "Reaktionen",
        sum(Reaktionen) as Anzahl 
    from 
        wim_reactions_overview 
    where 
        Fraktion is not null 
        and project="' . $GLOBALS["active_project"] .'"
    union 
    select 
        "Politiker",
        count(distinct(`Twitter Handle`)) as Anzahl 
    from 
        wim_reactions_overview 
    where 
        Fraktion is not null 
        and Reaktionen > 0 
        and project="' . $GLOBALS["active_project"] . '";
';
    $result1 = $GLOBALS["conn"]->query($sql1);
    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Aspekt</th>
            <th>Anzahl</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result1->fetch_assoc()) {
        echo "<tr>
        <td>" . $row["Kontakte"] . "</td>
        <td>" . $row["Anzahl"] . "</td>
        </tr>\n\r";

        if ("Kontakte"==$row["Kontakte"]) {
            $kontakte=$row["Anzahl"]; 
        } elseif ("Politiker"==$row["Kontakte"]) {
            $Politiker=$row["Anzahl"]; 
        } elseif ("Reaktionen"==$row["Kontakte"]) {
            $Reaktionen=$row["Anzahl"]; 
        }
    }

    $counterMetaValue='s:403:"a:3:{i:0;a:3:{s:12:"counter_icon";s:15:"fa-mail-forward";s:13:"counter_value";s:5:"' 
        . $kontakte 
        . '";s:13:"counter_title";s:24:"Politiker*innen Kontakte";}i:1;a:3:{s:12:"counter_icon";s:17:"fa-mail-reply-all";s:13:"counter_value";s:2:"' 
        . $Reaktionen 
        . '";s:13:"counter_title";s:25:"Politiker*innen Antworten";}i:2;a:3:{s:12:"counter_icon";s:8:"fa-users";s:13:"counter_value";s:2:"' 
        . $Politiker 
        . '";s:13:"counter_title";s:15:"Politiker*innen";}}";';

    echo "</tbody></table>\n\r";

    $sql="update wp_postmeta set meta_value='".$counterMetaValue. "' where meta_id=527;";

    $result1 = $GLOBALS["conn"]->query($sql);

    echo "Update Counter Number MySQL result: " . $result1;
    
}

function showInteractionStatisticsTable() {
    $sql1="select 
    count(distinct(lower(ta.twitter_name))) as users,
	count(*) as interactions,
	count(distinct(wim_questions.id)) as questions
    from wim_twitter_answers as tans
    left join wim_twitter_accounts as ta on ta.`twitter_user_id`=tans.twitter_author_id
    left join wim_send_tweets as st on st.`conversation_id`=tans.`conversation_id`
    left join wim_questions on wim_questions.id=st.`question_id`
    where twitter_name is not null 
    and wim_questions.project='" . $GLOBALS["active_project"] . "';";
    $result1 = $GLOBALS["conn"]->query($sql1);
    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Users</th>
            <th>Interactions</th>   
            <th>Questions</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result1->fetch_assoc()) {
        echo "<tr>
        <td>" . $row["users"] . "</td>
        <td>" . $row["interactions"] . "</td>
        <td>" . $row["questions"] . "</td>
    </tr>\n\r";
    }
    echo "</tbody></table>\n\r";
}

function showUserInteractionStatisticsTable() {
    $sql1="
    select 
	    ta.twitter_name,
	    count(*) as interactions,
	    count(distinct(wim_questions.id)) as questions
    from 
        wim_twitter_answers as tans
        left join wim_twitter_accounts as ta on ta.`twitter_user_id`=tans.twitter_author_id
        left join wim_send_tweets as st on st.`conversation_id`=tans.`conversation_id`
        left join wim_questions on wim_questions.id=st.`question_id`
    where 
        twitter_name is not null
        and wim_questions.project='" . $GLOBALS["active_project"] . "'
    group by 
        lower(ta.twitter_name),twitter_author_id
    ;";
    $result = $GLOBALS["conn"]->query($sql1);
    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Twitter Name</th>
            <th>Interactions</th>
            <th>Questions</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result->fetch_assoc()) {
        echo "<tr>
        <td><a href=\"https://twitter.com/" . $row["twitter_name"]  . "\" target=\"_blank\">"  . $row["twitter_name"] .  "</a></td>
        <td>" . $row["interactions"] . "</td>
        <td>" . $row["questions"] . "</td>
    </tr>\n\r";
    }
    echo "</tbody></table>\n\r";
}

function showUserQuestionInteractionStatisticsTable() {
    $sql2="
    select 
	    ta.twitter_name,
	    wim_questions.question as question,
	    count(*) as interactions
    from 
        wim_twitter_answers as tans
        left join wim_twitter_accounts as ta on ta.`twitter_user_id`=tans.twitter_author_id
        left join wim_send_tweets as st on st.`conversation_id`=tans.`conversation_id`
        left join wim_questions on wim_questions.id=st.`question_id`
    where 
        twitter_name is not null
        and wim_questions.project='" . $GLOBALS["active_project"] . "'
    group by 
        lower(ta.twitter_name),
        wim_questions.id,
        twitter_author_id
    ;";
    $result2 = $GLOBALS["conn"]->query($sql2);

    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Twitter Name</th>
            <th>Questions</th>
            <th>Interactions</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result2->fetch_assoc()) {
        echo "<tr>
        <td><a href=\"https://twitter.com/" . $row["twitter_name"]  . "\" target=\"_blank\">"  . $row["twitter_name"] .  "</a></td>
        <td>" . $row["question"] . "</td>
        <td>" . $row["interactions"] . "</td>
    </tr>\n\r";
    }

    echo "</tbody></table>\n\r";
}

function showUserQuestioAnswerTypeStatisticsTable(){
    $sql3="
    select 
	    ta.twitter_name,
	    wim_questions.question as question,
	    tans.answer_evaluation,
	    count(question_id) as number
    from 
        wim_twitter_answers as tans
        left join wim_twitter_accounts as ta on ta.`twitter_user_id`=tans.twitter_author_id
        left join wim_send_tweets as st on st.`conversation_id`=tans.`conversation_id`
        left join wim_questions on wim_questions.id=st.`question_id`
    where 
        twitter_name is not null
        and wim_questions.project='" . $GLOBALS["active_project"] . "'
    group by lower(ta.twitter_name),wim_questions.id,twitter_author_id,tans.answer_evaluation;";
    $result3 = $GLOBALS["conn"]->query($sql3);

    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Twitter Name</th>
            <th>Questions</th>
            <th>Evaluations</th>
            <th>Numbers</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result3->fetch_assoc()) {
        echo "<tr>
        <td><a href=\"https://twitter.com/" . $row["twitter_name"]  . "\" target=\"_blank\">"  . $row["twitter_name"] .  "</a></td>
        <td>" . $row["question"] . "</td>
        <td>" . $row["answer_evaluation"] . "</td>
        <td>" . $row["number"] . "</td>
    </tr>\n\r";
    }

    echo "</tbody></table>\n\r";
}


function showAnswers() {
    decoratePage();

    $sql="
    select 
        q.question as question,
        q.answer as correct_answer,
        tac.twitter_name as userTwitterName,
        st.conversation_id as conversation_id,
        st.timestamp as question_timestamp,
        ta.id as answer_id,
        ta.timestamp as answer_timestamp,
        ta.answer_evaluation as evaluation,
        ta.twitter_answer_id as twitter_answer_id,
        ta.twitter_answer_text as userAnswer 
    from wim_twitter_answers as ta 
        left join wim_send_tweets as st on ta.conversation_id=st.conversation_id 
        left join wim_twitter_accounts as tac on ta.twitter_author_id=tac.`twitter_user_id`
        left join wim_questions as q on q.`id`=st.`question_id` 
    where 
        q.project='" . $GLOBALS["active_project"] . "'
    order by q.question;";
    $result = $GLOBALS["conn"]->query($sql);
    echo <<<EOT



    <table  id="answers" class="display">
        <thead><tr>
            <th>Posed</th>
            <th>Question</th>
            <th>Official Answer</th>
            <th>User Answer</th>
            <th>Evaluation</th>
            <th>User Twitter Name</th>
            <th>Answered</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result->fetch_assoc()) {
        echo "<tr>
        <td>" . $row["question_timestamp"] . "</td>
        <td><a href=\"https://twitter.com/WissenistMach10/status/" . $row["conversation_id"]  . "\" target=\"_blank\">" . $row["question"] . "</a></td>
        <td>" . $row["correct_answer"] . "</td>
        <td><a href=\"https://twitter.com/a/status/" . $row["twitter_answer_id"]  . "\" target=\"_blank\">" . $row["userAnswer"] . "</a></td>
        <td>"; ?>

        <form action="?command=updateEvaluation" method="POST">
            <select id="evaluation" name="evaluation">
                <option value="<?php echo $row["evaluation"]?>"><?php echo $row["evaluation"]?></option>
                <option value="not_evaluated">not_evaluated</option>
                <option value="not_an_answer">not_an_answer</option>
                <option value="correct">correct</option>
                <option value="incorrect">incorrect</option>
            </select>
            <input type="hidden" name="answer_id" value="<?php echo $row["answer_id"] ?>">  
            <input type="submit">
        </form>
        
        <?php echo "</td>
        <td><a href=\"https://twitter.com/" . $row["userTwitterName"]  . "\" target=\"_blank\">"  . $row["userTwitterName"] .  "</a></td>
        <td>" . $row["answer_timestamp"] . "</td>
</tr>\n\r";
    }

    echo "</tbody></table>\n\r";
}

function showFactsAndQuestions(){
    decoratePage();

    $sql="select id,topic,fact,concat('https://bit.ly/',shorturl) as bitly from wim_facts where project='" .  $GLOBALS["active_project"] . "';";
    $result = $GLOBALS["conn"]->query($sql);

    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Topic</th>
            <th>Fact</th>
            <th>ID</th>
            <th>Bitly Link</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $result->fetch_assoc()) {

        $sql2="select * from wim_questions where factid=" . $row["id"] . ";";
        $result2 = $GLOBALS["conn"]->query($sql2);

        echo "<tr>
        <td>" . $row["topic"] . "</td>
        <td>" . $row["fact"] . "<table>";

        while($row2 = $result2->fetch_assoc()) {
            echo "<tr><td>" . $row2["id"] . "</td><td>". $row2["question"] . "</td><td>". $row2["answer"] ."</td></tr>";
        }

       echo  "</table></td>
        <td>" . $row["id"] . "</td>
        <td><a href=\"" . $row["bitly"] . "\" target=\"_blank\">" .$row["bitly"].  "</a></td>
    </tr>\n\r";
    }

    echo "</tbody></table>\n\r";
}

function performV2TwitterAPICall($QueryURL) {
    $localSettings = $GLOBALS["settings"];
    $operation_mode=$GLOBALS["mode"];
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $QueryURL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer ' . $localSettings["bearer_token"]. ''
        ),
    ));
    if ("listCurlCommand" == $operation_mode) {
        echo "test mode. uery URL:" . $QueryURL . '<br>';
        $response = "";
    } else {
        $response = curl_exec($curl);
        curl_close($curl);
    }
    return $response;
}

function getTwitterInfos() {
    decoratePage();
    echo "Number of followers: " . getTwitterFollowers() . "</br>";
    echo "Anzahl Twitter accounts without user id: " . getTwitterUsersID() . "</br>";
    echo "Anzahl Twitter accounts without names: " . getTwitterUsersName() . "</br>";
    echo "Number of Answers: " . getAnswers() . "</br>\n\r";
    echo "Number of Helpers: " . getTwitterHelpers() . "</br>\n\r";
}

function getTwitterInfos_Followers() {
    decoratePage();
    echo "Number of followers: " . getTwitterFollowers() . "</br>";
}

function getTwitterInfos_UsersID() {
    decoratePage();
    echo "Anzahl Twitter accounts without user id: " . getTwitterUsersID() . "</br>";
}

function getTwitterInfos_UsersName() {
    decoratePage();
    echo "Anzahl Twitter accounts without names: " . getTwitterUsersName() . "</br>";
}

function getTwitterInfos_Answers() {
    decoratePage();
    echo "Number of Answers: " . getAnswers() . "</br>\n\r";
}

function getTwitterInfos_Helpers() {
    decoratePage();
    echo "Number of Helpers: " . getTwitterHelpers() . "</br>\n\r";
}

function getTwitterInfos_MentionsFromConversaton_ID() {
    decoratePage();
    echo "Number of Mentions: " . getTwitterMentionsFromConversaton_ID() . "</br>\n\r";
}

function getTwitterInfos_RelevantRetweets() {
    decoratePage();

    $sql="select questionid from `wim_what_question_when` where 
            `wim_what_question_when`.`when_to_ask` <= curdate() 
	        and `wim_what_question_when`.`when_to_stop_asking` >= curdate()
            and project='" .  $GLOBALS["active_project"] . "';"; 
    echo $sql;
    $res = $GLOBALS["conn"]->query($sql);
    $questionid="no Id found";
    while($row = $res->fetch_assoc()) {
        $questionid = $row["questionid"];
    }
    echo <<<EOT
    <form action="?command=getTwitterInfos_RelevantRetweets" method="POST">
    <p>Question ID: <input type="text" name="question_id" size="6" value="$questionid"/></p>
    <p><input type="submit"/></p>
    </form>
    EOT;
    if (isset($_POST["question_id"])) {
        echo "Number of Retweets für Frage: " . $_POST["question_id"] . " -> " . getTwitterRelevantRetweets($_POST["question_id"]) . "</br>\n\r";
    }
}


function getTwitterRelevantRetweets($question_id) {

    $numberOfRetweets=0;
    $numberOfDirectRetweet=0;

    $sql="select 
            when_to_ask, when_to_stop_asking 
        from wim_what_question_when where questionid='" . $question_id . "' 
        and project='" .  $GLOBALS["active_project"] . "';"; 
    if ("wim"== $GLOBALS["active_project"]) {
        $questionHashtagProject="WIM";
    } else {
        $questionHashtagProject="SDLT";

    }
    echo $sql;
    $res = $GLOBALS["conn"]->query($sql);
    while($row = $res->fetch_assoc()) {
        $dateToProcessStart=$row["when_to_ask"];$dateToProcessStop=$row["when_to_stop_asking"];
        $dateToProcess=$dateToProcessStart;
        $questionHashtag = $questionHashtagProject . substr($dateToProcess,5,2) . substr($dateToProcess,8,2);
        while (strtotime($dateToProcess) < strtotime($dateToProcessStop)) {
            $dateToProcess = date("Y-m-d", strtotime("+1 day", strtotime($dateToProcess)));
            $questionHashtag .= ' OR ' . $questionHashtagProject . substr($dateToProcess,5,2) . substr($dateToProcess,8,2);
        }
    }

    if ( ("WIM" != $questionHashtag) || ("SDLT" != $questionHashtag)) {
        echo '<a href="https://twitter.com/hashtag/' . $questionHashtag . '" target="_blank">' . "https://twitter.com/hashtag/" . $questionHashtag ."</a><br>";
        // example query
        // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/request/873942-08af78dc-ac7b-44a2-8ab8-65f82e4de41e
        $QueryURL = "https://api.twitter.com/2/tweets/search/recent?expansions=author_id,referenced_tweets.id&max_results=100&query=";
        $QueryURL .= urlencode($questionHashtag);

        echo $QueryURL . '<br>';

        $response=performV2TwitterAPICall($QueryURL);
        // example response
        // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/example/873942-0831500a-eb24-466b-b354-874ecc6dbdb4
        $res=json_decode($response,true);
        $sql='insert ignore into wim_send_tweets (project,question_id,conversation_id) values ';
        $retweets = array();
        $rootTweet_id=0;
        if (isset($res["data"])) {
            foreach ($res["data"] as $r) {

                if (isset($r["referenced_tweets"])) {
                    $retweets[] = array (
                        "tweet_id" => $r["id"],
                        "ref_tweet" => $r["referenced_tweets"]["0"]["id"],
                        "tweet_type" => $r["referenced_tweets"]["0"]["type"]
                    );
                    echo '<a href="https://twitter.com/dummy/status/' . $r["id"] . '" target="_blank">' . $r["id"] . '</a> Type: ' . $r["referenced_tweets"]["0"]["type"]. '<br>';
                    $numberOfRetweets++;
                } elseif ( ("1377944760778956802" == $r["author_id"] ) || ("1535957327307735043" == $r["author_id"])) {
                    $rootTweet_id= $r["id"];
                    echo '<a href="https://twitter.com/dummy/status/' . $r["id"] . '" target="_blank">' . $r["id"] . '</a> Type: original posting<br>';

                }
            }
        }

        foreach($retweets as $retweet) {
            //if ($retweet["ref_tweet"] == $rootTweet_id ) {
                $sql .= "('" .  $GLOBALS["active_project"] . "','" . $question_id . "','" . $retweet["tweet_id"] . "'),";
                $numberOfDirectRetweet++;
            //}
        }


        if (0!=$rootTweet_id) {
            $sql .= "('" .  $GLOBALS["active_project"] . "','" . $question_id . "','" . $rootTweet_id . "'),";
            // $sql ="update ignore `wim_what_question_when` set conversation_id='" . $rootTweet_id. "' where when_to_ask='" . $dateToProcess ."';";
            // $res2 = $GLOBALS["conn"]->query($sql);
        } else {
            echo "no original post were found for this question ID, post my be older than 7 days<br>";
        }

        $sql = rtrim($sql,',') . ";";
        echo $sql . '<br>'; 

        $res2 = $GLOBALS["conn"]->query($sql);


    } else {
      echo "question ID not valid<br>";  
    } 

    return $numberOfDirectRetweet;
}

function getAnswers() {
    $sql = "SELECT * FROM wim_send_tweets where conversation_id is not null and  date(timestamp) > curdate() -10 and project='" .  $GLOBALS["active_project"] . "';"; 
    echo $sql . '<br>';
    $result = $GLOBALS["conn"]->query($sql);
    $numberOfAnswers=0;
    while($row = $result->fetch_assoc()) {
        $conversation_id= $row["conversation_id"];
        // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/request/873942-87a50f67-3d04-46c2-965e-4acae860ba89
        $QueryURL = "https://api.twitter.com/2/tweets/search/recent?query=conversation_id:";
        $QueryURL .= $conversation_id;
        $QueryURL .="&expansions=author_id";
        echo $QueryURL . '<br>';
        $response=performV2TwitterAPICall($QueryURL);
        $res=json_decode($response,true);
        if ($result->num_rows > 0 && isset($res["data"])) {
            foreach ($res["data"] as $r) {
                $sql = "INSERT ignore INTO wim_twitter_answers (id, project, conversation_id, twitter_answer_id, twitter_author_id, twitter_answer_text) VALUES (NULL,'" . $GLOBALS["active_project"] . "','" .$conversation_id . "', '" . $r["id"] . "', '" . $r["author_id"] . "', '" . $r["text"] . "');";
                $res2 = $GLOBALS["conn"]->query($sql);
                $numberOfAnswers++;
            }
        }
    }
    return $numberOfAnswers;
}


function getTwitterUsersID() {
    $sql = "select count(*) as anzahl from wim_twitter_accounts where twitter_user_id is null  and project='" .  $GLOBALS["active_project"] . "';"; 
    $result = $GLOBALS["conn"]->query($sql);   
    while($row = $result->fetch_assoc()) {
        $anzahl_accounts=$row["anzahl"];
    }
    
    // Twitter API does not accept more than 100 names at once
    for ( $offset=0;$offset < $anzahl_accounts;$offset+=10) {
        echo "processing offset: $offset </br>";
        // example query with API
        // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/request/873942-6429f4c1-ff41-4e4f-a067-ad759e23918b
        $QueryURL = "https://api.twitter.com/2/users/by?usernames=";
        $sql = "SELECT * FROM wim_twitter_accounts where twitter_user_id is null and project='" . $GLOBALS["active_project"]  . "' limit $offset, 10;"; 
        $result = $GLOBALS["conn"]->query($sql);
        if ($result->num_rows > 0) {
            $first_item=true;
            while($row = $result->fetch_assoc()) {
                if (true == $first_item) {
                    $first_item=false;
                } else {
                    $QueryURL .= ",";
                }
                $QueryURL .= $row["twitter_name"] ;
            }
            // example response from API
            // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/example/873942-693d0264-ff9b-4a60-8a10-4638c60e32fe
            $response=performV2TwitterAPICall($QueryURL);
            $res=json_decode($response,true);
            if (isset($res["errors"])) {
                echo '<pre>';
                print_r($res); echo '<br>';
                echo '</pre>';
            } 
                foreach ($res["data"] as $r) {
                    $sql = "update wim_twitter_accounts set twitter_user_id='" . $r["id"] . "' where lower(twitter_name)='" . strtolower($r["username"]) . "'";
                    echo "&nbsp;&nbsp;processing: " . $r["username"] . ", ". $r["id"] . ". " . $sql . '<br>';
                    $result = $GLOBALS["conn"]->query($sql);
                }
            
        } else {
            echo "0 results";
        }
    }
    return $anzahl_accounts;
}

function getTwitterUsersName() {
    $sql = "select count(*) as anzahl from wim_twitter_accounts where twitter_name is null and project='" .  $GLOBALS["active_project"] . "';"; 
    $result = $GLOBALS["conn"]->query($sql);   
    while($row = $result->fetch_assoc()) {
        $anzahl_accounts=$row["anzahl"];
    }
    
    // Twitter API does not accept more than 100 IDS at once
    for ( $offset=0;$offset < $anzahl_accounts;$offset+=100) {
        echo "processing offset: $offset </br>";
        // example query with API
        // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/request/873942-20388646-9c47-472a-a65a-342838ad690b
        $QueryURL = "https://api.twitter.com/2/users?ids=";
        $sql = "SELECT * FROM wim_twitter_accounts where twitter_name is null and project='" .  $GLOBALS["active_project"] . "' limit $offset, 100;"; 
        $result = $GLOBALS["conn"]->query($sql);
        if ($result->num_rows > 0) {
            $first_item=true;
            while($row = $result->fetch_assoc()) {
                if (true == $first_item) {
                    $first_item=false;
                } else {
                    $QueryURL .= ",";
                }
                $QueryURL .= $row["twitter_user_id"] ;
            }
            echo $QueryURL; 
            // example response from API
            // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/example/873942-a4807fc7-f86b-4a3c-8b0c-f4c9a4ea8921
            $response=performV2TwitterAPICall($QueryURL);
            $res=json_decode($response,true);
            foreach ($res["data"] as $r) {
                echo "&nbsp;&nbsp;processing: " . $r["id"] . ", ". $r["id"] . "</br>\n\r";
                $sql = "update wim_twitter_accounts set twitter_name='" . $r["username"] . "', name='" . remove_emoji($r["name"]) . "' where twitter_user_id='" . $r["id"] . "'";
                echo $sql;
                $result = $GLOBALS["conn"]->query($sql);
            }
        } else {
            echo "0 results";
        }
    }
    return $anzahl_accounts;
}

function getTwitterFollowers(){
    $localSettings = $GLOBALS["settings"];

    // example query from API
    // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/request/873942-6636a97a-6cbf-4afa-897e-1e069b4b7d64
    $url = 'https://api.twitter.com/1.1/followers/ids.json';
    $getfield = '';
    $requestMethod = 'GET';

    $twitter = new TwitterAPIExchange($localSettings);
    
    // example respoonse from API
    // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/example/873942-91606ea1-e520-494c-a43e-4800e4c54ebc
    $response = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();

    $res=json_decode($response,true);

    $list1="('";
    $list2="";
    $numberOfFollowers=0;
    foreach ($res["ids"] as $r) {
        $list1.= $r . "','";
        $list2.="(".$r.",'normal_user',1,0),";
        $numberOfFollowers++;
    }
    $list1=rtrim(rtrim($list1,"'"),",") .")";
    $list2=rtrim($list2,",");
    $sql1 = "update wim_twitter_accounts set twitter_follower='1' where twitter_user_id in " . $list1 . ";";
    $result = $GLOBALS["conn"]->query($sql1);
    $sql2 = "insert ignore into wim_twitter_accounts (twitter_user_id,type,twitter_follower,active) values " . $list2 ;
    $result2 = $GLOBALS["conn"]->query($sql2);

    return $numberOfFollowers;
}

function getTwitterHelpers(){
    
    $QueryURL = 'https://api.twitter.com/2/users/1377944760778956802/mentions?expansions=author_id&tweet.fields=entities';

    $response=performV2TwitterAPICall($QueryURL);

    $res=json_decode($response,true);

    $numberOfHelpers=0;
    foreach ($res["data"] as $r) {
        // echo "author id: " . $r["author_id"] . " text:  " . $r["text"] . "</br>";
        if ( strpos($r["text"], "#helfer" ) !== false) {
            $numberOfHelpers++;
            $sql = "update wim_twitter_accounts set twitter_helper='1' where twitter_user_id = " . $r["author_id"] . ";";
            $result = $GLOBALS["conn"]->query($sql);
        }
    }

    return $numberOfHelpers;
}

function BuilTwitterStatusFromPOSTArguments() {

    echo '<table id="table_id" class="display"> <thead> <tr> <th> Twitter Status </th> </tr> <tbody>';
    if ("wim" == $GLOBALS["active_project"]) {
        $twitterStatus = "#WIM";
    } elseif ("sdlt" == $GLOBALS["active_project"]) {
        $twitterStatus = "#SDLT";
    }
    $twitterStatus .= date('md') . " " . $_POST["question"] . " " . $_POST["hint"]; 
    $tweetLength=strlen($twitterStatus);
    $twitterStatus = '<a href="https://twitter.com/intent/tweet?text=' . urlencode($twitterStatus) . '" target="_blank">' . $twitterStatus . "</a>";
    echo "<tr><td>$twitterStatus</td></tr>";
    echo "</tbody></table>";
    echo "Tweet length: " . $tweetLength;

}


function getTwitterMentionsFromConversaton_ID(){

    $sql = "
        select count(*) as anzahl 
        from wim_send_tweets 
        where 
            conversation_id is not null 
            and question_id is not null 
            and (recipients is null or recipients = '')
            and project='" . $GLOBALS["active_project"]  . "'
        ;"; 
    echo $sql . '<br>';
    $result = $GLOBALS["conn"]->query($sql);   
    while($row = $result->fetch_assoc()) {
        $anzahl_send_tweets=$row["anzahl"];
    }

    $numberOfMentions=0;
    $values='';
    for ( $offset=0;$offset <= $anzahl_send_tweets;$offset+=100) {
        
        ob_end_flush();
        echo "Working on offset: " . $offset . '<br>';
        ob_start();

        // https://meinungsmeister.postman.co/workspace/Private-Ground~e20f25e1-3255-45ae-8b8f-09039c6087b9/request/873942-0a516564-7218-4ad5-a959-f00897ae4132

        $sql="
            select conversation_id,question_id 
            from wim_send_tweets 
            where 
                conversation_id is not null 
                and question_id is not null 
                and (recipients is null or recipients = '')
                and project ='" .  $GLOBALS["active_project"] . "'
            order by conversation_id desc limit " . $offset . ",100;";

        $result = $GLOBALS["conn"]->query($sql);

        $ids='';$questions_conversations = array();
        while($row = $result->fetch_assoc()) {
            $ids.=$row["conversation_id"].',';
            $questions_conversations[$row["conversation_id"]] = $row["question_id"];
        }

        $QueryURL='https://api.twitter.com/2/tweets?ids=' . rtrim($ids,",") .'&tweet.fields=created_at,author_id&expansions=entities.mentions.username';

        echo $QueryURL . '<br>';
        $response=performV2TwitterAPICall($QueryURL);

        $res=json_decode($response,true);

        if(!empty($res["data"])) {
            foreach ($res["data"] as $r) {
                if(!empty($r["entities"]["mentions"])) {
                    foreach($r["entities"]["mentions"] as $s) {
                        $values .=  "('" . $GLOBALS["active_project"] . "','" . substr($r["created_at"],0,10) . "','" . substr($r["created_at"],11,8) . "','" . $s["username"] . "','" . $r["author_id"] . "','" . $r["id"] . "','" . $questions_conversations[$r["id"]] ."'),";
                        $numberOfMentions++;
                    }
                }
            }
        }

        echo ". Got " . $numberOfMentions . " mentions so far.<br>";
    }

    $values = rtrim($values,",") . ';';
    $sql='insert ignore into wim_twitter_mentions(project,date,time,twitter_name_receiver,twitter_id_sender,conversation_id,question_id)
    values' . $values;

    $result = $GLOBALS["conn"]->query($sql);

    $sql = "truncate wim_website_politician_quoted_tweet_suggestions;";
    $result = $GLOBALS["conn"]->query($sql);  

    $sql = "insert into wim_website_politician_quoted_tweet_suggestions(project,twitter_name,date,time,question_id) select project,twitter_name_receiver,date,time,question_id  from wim_twitter_mentions;";
    $result = $GLOBALS["conn"]->query($sql);  
    
    return $numberOfMentions;

}

function remove_emoji($string) {

    // Match Emoticons
    $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clear_string = preg_replace($regex_emoticons, '', $string);

    // Match Miscellaneous Symbols and Pictographs
    $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clear_string = preg_replace($regex_symbols, '', $clear_string);

    // Match Transport And Map Symbols
    $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clear_string = preg_replace($regex_transport, '', $clear_string);

    // Match Miscellaneous Symbols
    $regex_misc = '/[\x{2600}-\x{26FF}]/u';
    $clear_string = preg_replace($regex_misc, '', $clear_string);

    // Match Dingbats
    $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
    $clear_string = preg_replace($regex_dingbats, '', $clear_string);

    return $clear_string;
}

function showSendProtocol(){
    if (isset($_GET["protocol_id"])) {
        showOneRawJSONSendProtocol($_GET["protocol_id"]);
    } else {
        showListOfSendProtocols();
    }
}


function showOneRawJSONSendProtocol($protocol_id) {      
    echo <<<EOT
    <form id="myForm" action="https://jsonformatter.curiousconcept.com/" method="post">
    EOT;

    $sql="select * from wim_send_tweets where id=" . $protocol_id . ";";
    $res = $GLOBALS["conn"]->query($sql);
    while($row = $res->fetch_assoc()) {
        $twitter_api_response = $row["twitter_api_response"];
    }

    echo '<input type="hidden" name="data" value="' . htmlentities($twitter_api_response).'">';
  
    echo <<<EOT
    </form>
    <script type="text/javascript">
    document.getElementById('myForm').submit();
    </script>
    EOT;

}

// showing raw JSON allows the browser extension to beautify the output
function showOneRawJSONSendProtocol1($protocol_id) {
    $sql="select * from wim_send_tweets where id=" . $protocol_id . ";";
    $res = $GLOBALS["conn"]->query($sql);
    while($row = $res->fetch_assoc()) {
        echo  $row["twitter_api_response"];
    }
}

function showListOfSendProtocols(){
    decoratePage();
    $sql="select * from wim_send_tweets and project='" .  $GLOBALS["active_project"] . "';";
    $res = $GLOBALS["conn"]->query($sql);
    $localWIM_URL_Subdirectory=$GLOBALS["wim_url_subdirectoy"];

 
    echo <<<EOT
    <table  id="table_id" class="display">
        <thead><tr>
            <th>Timestamp</th>
            <th>ID</th>
            <th>recipients</th>
            <th>first degree id</th>
            <th>second degree info</th>
            <th>hashtag</th>
            <th>question</th>
            <th>question id</th>
            <th>conversation id</th>
            <th>Status</th>
            <th>show Protocol</th>
            <th>Process protocol</th>
        </tr></thead>
        <tbody>
    EOT;

    while($row = $res->fetch_assoc()) {
        echo "<tr>
        <td>" . $row["timestamp"]  .  "</td>
        <td>" . $row["id"] . "</td>
        <td>" . $row["recipients"] . "</td>
        <td>" . $row["first_degree_user_id"] . "</td>
        <td>" . $row["second_degree_user_info"] . "</td>
        <td>" . $row["hashtag"] . "</td>
        <td>" . $row["question"] . "</td>
        <td>" . $row["question_id"] . "</td>
        <td><a href=\"http://www.twitter.com/a/status/"  . $row["conversation_id"] . "\" target=\"_blank\">". $row["conversation_id"] ."</a></td>
        <td>" . $row["status"] .  "</td>
        <td><a href=\"?command=send_protocol&protocol_id=" . $row["id"]  . "\" target=\"_blank\"> show JSON </a></td>
        <td><a href=\"?command=parse_send_protocol&protocol_id=" . $row["id"]  . "\" target=\"_blank\">process protocol</a></td>
        </tr>\n\r";
    }

    echo "</tbody></table>\n\r";

}


function sendTwitterStatus($twitterStatus) {
    $twitter = new TwitterAPIExchange($GLOBALS["settings"]);
    /** URL for REST request, see: https://developer.twitter.com/en/docs/twitter-api/v1  **/
    /** POST fields required by the URL above. See relevant docs as above **/
    $postfields = array(
        'status' => $twitterStatus,
        'trim_user' => true
    );
    $url = 'https://api.twitter.com/1.1/statuses/update.json';
    $requestMethod = 'POST';
    $send_protocol=$twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();
    return $send_protocol;
}

function showTweetPreparation() {
    decoratePage();

    $operation_mode=$GLOBALS["mode"];
    $fact_root_URL=$GLOBALS["hint_root_url"];



    $topic="no topic";
    $info="no info";
    $question="no question found";
    $answer= "no answer found";
    $shorturl="no factid found";
    $sql = "
    SELECT * 
    FROM wim_what_question_when 
        join wim_questions on wim_what_question_when.questionid = wim_questions.id 
        join wim_facts on wim_facts.id = wim_questions.factid
    WHERE 
	    `wim_what_question_when`.`when_to_ask` <= curdate() 
	    and `wim_what_question_when`.`when_to_stop_asking` >= curdate() 
        and wim_what_question_when.project='" .  $GLOBALS["active_project"] . "';
    "; 
    $res = $GLOBALS["conn"]->query($sql);
    while($row = $res->fetch_assoc()) { // while should be removed, as only one answer is expected, otherwise -> error
        $topic = $row["topic"];
        $info = $row["fact"];
        $question=$row["question"];
        $questionid=$row["questionid"];
        $answer= $row["answer"];
        $shorturl=$row["shorturl"];
    }


    echo <<<EOT
    Topic: $topic </br> Info: $info </br>
    <form action="?command=showNextTweet" method="POST">
    <p>Question: <input type="text" name="question" size="156" value="$question"/></p>
    <input type="hidden" id="questionid" name="questionid" value="$questionid">

    <p>Answer: $answer</p>
    EOT;
    if ("wim"== $GLOBALS["active_project"]) {
        echo '<p>Link to hint: <input type="text" name="hint" size="98" value=" Spielen Sie mit, antworten Sie bitte auf diesen Tweet! Hinweise zum Quiz und zu dieser Frage: $fact_root_URL$shorturl"/></p>';
    } else {
        echo '<p>Link to hint: <input type="text" name="hint" size="98" value=" Répondez à ce tweet ! Indices sur le jeu et cette question : https://bit.ly/'. $shorturl . '"/></p>';
    } 
  
    
    $sql= '
    select
	    concat("@",ta_first.twitter_name) as "twitter_reference",
	    concat("@", group_concat(ta_second.twitter_name separator " @")) as "reference_add_on"
    from 
        wim_twitter_accounts as ta_first 
        left join wim_twitter_accounts as ta_second on ta_first.id=ta_second.retweet_user_id and ta_first.twitter_follower=true 
    where 
        ta_first.active=1 
        and ta_second.active=1
        and ta_first.project="' .  $GLOBALS["active_project"] . '"
    group by 
        ta_first.twitter_name, 
        ta_second.group_id'; 

    if ("listNextPost" == $operation_mode ) {
        $sql="select * from wim_facts where 0=1;";
    }

    echo <<<EOT

    <p><input type="submit" value="$operation_mode"/></p>
    </form>
    EOT;

}



function decoratePage() {

    echo <<<EOT
    <!DOCTYPE html>
    <html>
    <head>

    </head>
    <body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>    
    <script>
    $(document).ready( function () {
        $('table.display').DataTable({"pageLength": 100});
    } );  
    </script>


    <style>
    body 
        {background-color: powderblue;
        margin:10px;
        }
    h1   {color: blue;}
    p    {color: red;}

    UL.nav > LI {
        list-style: none;
        float: left;
        position: relative;
        height: 24px; /* height included for IE 7 */
    }
    
    UL.nav UL{
        left: -10000px;
        position: absolute;            
    }
    
    UL.nav > LI:hover UL{
        left: 0;
        top: 24px; /* IE7 has problems without this */
    }

    </style>
    EOT;

    echo "<h1>" . $GLOBALS["active_project"] . "</h1> change to -> ";
    $sql="SELECT project FROM `wim_projects`;";
        $resproj = $GLOBALS["conn"]->query($sql);
        while($row = $resproj->fetch_assoc()) {
	        echo '<a href="?setProject=' . $row["project"] . '">' . $row["project"] . "</a> ";
    }

    echo <<<EOT
    <table border="1"><tr>
    <td><a href="/phpmyadmin" target="_blank">DB Access</a></td>
    <td><a href="/te-admin/index.php">Tweet preparation</a></td>
    <td>
    <ul class="nav">
        <li>Get Twitter Info over API
            <ul>
                <li><a href="?command=getTwitterInfos_Followers">Followers</a></li>
                <li><a href="?command=getTwitterInfos_UsersName">Users Name</a></li>
                <li><a href="?command=getTwitterInfos_UsersID">Users ID</a></li>
                <li><a href="?command=getTwitterInfos_RelevantRetweets">retweets</a></li>
                <li><a href="?command=getTwitterInfos_MentionsFromConversaton_ID">mentions</a></li>
                <li><a href="?command=getTwitterInfos_Answers">Answers</a></li>
                
            </ul>
        </li>   
    </ul> 
    </td>
    <td>
    <ul class="nav">
        <li>Show informations from DB
            <ul>
                <li><a href="?command=showStatisticsOverview">Statistics</a></li>
                <li><a href="?command=showFactsAndQuestions">Facts and Questions</a></li>
                <li><a href="?command=showAnswers">Answers</a</li>
            </ul>
        </li>
    </ul>
    </td>
    </tr></table>

    <br> 
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>


    EOT;
}


$conn->close();

?>
</body>
</html>
