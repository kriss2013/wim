[[_TOC_]]
# What is WIM

WIM stands for "Wissen ist Macht". A German serious game aiming at teaching users something about a particular topic by asking them questions on Twitter once a day. Please [read here](https://gitlab.com/kriss2013/wim/-/wikis/home) for more information regarding the genesis of this idea and [here](https://gitlab.com/kriss2013/wim/-/wikis/description-of-the-game-mechanics) to learn more about the game itself

if you want to contribute install a local dev env ([see below](https://gitlab.com/kriss2013/wim/-/tree/master#local-dev-env)) and look head first in the [issue list](https://gitlab.com/kriss2013/wim/-/issues)!

# Functions of the prototype
index.php file for the admin functions is in the directory "wim-admin". may be accessible through 0.0.0.0/wim-admin

At the moment we only have a design prototype aimed at exploring the solution space
- to send a batch of tweet, one need to call the GUI and send manually. This way we will have no problems with bugs sending to much
- All the fields in the form are comming from the DB, but can be edited on the fly before sending. Editings are NOT stored in the DB
- The DB table "what_question_when" in the DB selects which question populate the form based on the present day (we want to ask a different question every day)
- the recipients are also comming from the DB and only appear in the field if they are marked as "active" in the DB
- there is no editor for the DB, please use f.i. phpmyadmin or any other DB tool. 
- there is a test mode which does not use the Twitter API. The settings is in the configuration table, how to set the settings is also there. Use it to test the workflow. The send button changes according to the mode, this way one always which power one has, be carefull in "sending" mode!
- twitter accounts have a type. It is not used by the software yet, it is for us to classify the accounts. Expect the account table to get more such info fields in the future.
- when the send button is pressed, a log will be written in the send_protocol table. the content of the log depends on the mode. In "sending" mode, the JSON answer from Twitter is saved there. In test mode a mini JSON informing that we are in test mode is saved.
- the menu "send protocol" lists all logs. by clicking on a log, the JSON is displayed in a new browser window. Please use a browser extension to "beautify" the result. for chrome "JSON viewer" works well
- following workflow is implemented: "first degree" contacts (followers) become a tweet with the question. Those who moreover have associated "second degree" contacts (not followers), become several tweets with a couple of "@mention" in order to retweet as a human. Only a couple of "@mention" per Tweet because it counts in the overall Status length. Serveral Tweets in order to allow active people to reach a lot of second degree contacts. This is done to overcome the Tweeter quality filter preventing bot to contact users without any connection at all. The associtation between first and second degree usres is static in the DB, this way users may choose their second degree contacts.
- added several admin function as clickable links to deal with the twitter answers:
    - enhance users table with twitter User ID, necessary to easily maps the answerd afterwards
    - enhance user table with information wether they are following the bot or not (see point above) for the rationale)
    - fetch tweeter answers and mapp them to users
    - display answers next to the question. Enhance the DB (not the GUI) to evaluate the answer (correct, ot not, or even not an answer at all)
- added primitve statiscs


## screen shots

- Send Form, prefilled with daily question, corresponding hints and the list of recipients:

![Send Form](./doc/sendform.png)
- Overview of all send protocols:

![Send Protocol Overview](./doc/sendprotocoloverview.png)
- Detailed view of a send protocol, the view is formated by a chrome extension:

![Send Protocol](./doc/sendprotocol.png)
- Statistics:

![Statitics](./doc/statistics.png)
- Answers:

![Answers](./doc/answers.png)


# local dev env 
you need to get credentials to access the Twitter API. Then for easy dev settings (and maybe easy deployment?) we use a dockerized LAMP Stack.

## Twitter API settings

it seems so, that one needs to open a twitter developer account to get API Keys (https://developer.twitter.com/). It seems that even without approval, one can start creating twitter developer projects and start richt away. Please look around in developer console to know how to do all aof this.

here is a mapping described between the various API key/token and the variables used in the query: https://developer.twitter.com/en/docs/labs/covid19-stream/quick-start
for us:
```
    'oauth_access_token' =>   "Access token" in the Twitter developer console
    'oauth_access_token_secret' => "Access token secret" in the Twitter developer console
    'consumer_key' =>  "API Key" in the Twitter developer console 
    'consumer_secret'=> "API secret key" in the Twitter developer console
```
## LAMP Stack

after several unsucesfull attempts, we found that this stack works under OSX: https://hub.docker.com/r/mattrayner/lamp. You may need anothe stack if this one does not work, you than may need to suvsteancially adapt the "docker run" command below.

we use chapter III from the ["Twelve-Factor App"](https://www.12factor.net/config) paradigma for the configuration. It says, that the config should be done using environment variables. Please set the following environemnt variables before running docker
```bash
# the local directory where you are developing
export DEVPATH=devpath
# "Access token" in the Twitter developer console
export OAUTH_ACCESS_TOKEN=your_token
# "Access token secret" in the Twitter developer console
export OAUTH_ACCESS_TOKEN_SECRET=your_oauth_access_token_secret
# "API Key" in the Twitter developer console
export CONSUMER_KEY=your_consumer_key
# "API secret key" in the Twitter developer console
export CONSUMER_SECRET=your_consumer_secret
# alternative way to access API
export BEARER_TOKEN=your_bearer_token
# Password for MYSQL, has to be configured after first docker run 
export WIM_DB_PASSWORD=your_wim_db_password
```

then run this from a shell which has the environment variables active:
```bash
docker run -p "80:80" -p "443:443" -p "3306:3306" \
--name WIM \
-v "${DEVPATH}/app:/app" \
-v "${DEVPATH}/var/lib/mysql:/var/lib/mysql" \
--env OAUTH_ACCESS_TOKEN \
--env OAUTH_ACCESS_TOKEN_SECRET \
--env CONSUMER_KEY \
--env CONSUMER_SECRET \
--env BEARER_TOKEN \
--env WIM_DB_PASSWORD \
mattrayner/lamp:build-189-1804-php7
```
**IMPORTANT** when you run the docker command for the first time, or if you removed the mysql files under /var/lib/mysql, you will get the admin password written on the console. Please write it down, as it will be shown only once. Use that password to configure a DB user and then set the env variables with this user credentials and restart the container to take them into account. If you do not do that, the frontend will not be able to communicate with the backend.

### enabling HTTPS inside the container
based on https://www.webhosterwissen.de/know-how/eigener-webserver/tutorial-apache-lets-encrypt-fuer-ssl-schutz/
```apache
export DOMAIN=klimawissen-ist-macht.de

printf "
<VirtualHost *:80>
    ServerAdmin admin@$DOMAIN
    ServerName $DOMAIN
    ServerAlias www.$DOMAIN
    Redirect permanent / https://klimawissen-ist-macht.de/
    DocumentRoot /var/www/html
    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" | tee /etc/apache2/sites-available/$DOMAIN.conf
```
Let's Encrypt certbot installieren
```bash
add-apt-repository ppa:certbot/certbot
apt -y install python-certbot-apache
```
SSL-Zertifikate für klimawissen-ist-macht.de und www.klimawissen-ist-macht.de beziehen
```bash
certbot --apache -d www.klimawissen-ist-macht.de -d klimawissen-ist-macht.de
```
activate the site within apache
```bash
a2ensite $DOMAIN.conf
service apache2 reload
```
useful commands
```bash
a2dissite $DOMAIN.conf
service apache2 reload
 
a2ensite 000-default.conf
service apache2 reload
 
a2dissite 000-default.conf
service apache2 reload
```
renewal
```bash
certbot renew  // may be before certbot renew --dry-run to see if it is necessary
service apache2 reload
```
testing the result:
https://www.ssllabs.com/ssltest/analyze.html?d=www.klimawissen-ist-macht.de

## Using the Twitter API
got some inspiration from: https://iag.me/socialmedia/build-your-first-twitter-app-using-php-in-8-easy-steps/

this must be installed in the app folder: https://github.com/J7mbo/twitter-api-php -> requires an installed and activabel cURL on the host

# WP Theme
- OceanWP (+ Ocean Extra?) https://oceanwp.org/

# WP Plugins
- WP Data Access, both https://code-manager.com/ and https://wpdataaccess.com/
- GDPR http://cookielawinfo.com/
- Guaven SQL Charts http://guaven.com/
- Counter Number Showcase https://www.wpshopmart.com/
- Very Simple Contact Form https://www.guido.site/, together with "amr shortcode any widget" http://webdesign.anmari.com/
- Easy Update Manager https://easyupdatesmanager.com/
